'''
Created on Oct 9, 2019

@author: HEMEL
'''
import sys
from PyQt5.QtWidgets import QApplication

from gui.App import MyApp

if __name__=='__main__':
    app=QApplication(sys.argv)
    ex=MyApp()
    sys.exit(app.exec_())