'''
Created on Oct 9, 2019

@author: HEMEL
'''
from PyQt5.QtWidgets import QMainWindow
from PyQt5.Qt import QPushButton, QMessageBox, QLineEdit, QLabel, QAction
from PyQt5.QtCore import pyqtSlot
from cProfile import label

class MyApp(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.title="simple PyQt gui window"
        self.left=100
        self.top=100
        self.width=640
        self.height=480
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage("Message in status bar!")
        
        #Create menu
        mainMenu=self.menuBar()
        fileMenu=mainMenu.addMenu('File')
        editMenu=mainMenu.addMenu('Edit')
        viewMenu=mainMenu.addMenu("View")
        toolMenu=mainMenu.addMenu('Tool')
        helpMenu=mainMenu.addMenu('Help')
        
        #add exit option to the file menu
        exitButton =QAction(  'Exit',self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip("Exit application")
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)
        
        #create weveral lable with absolute positioning  
#         self.label=QLabel('python',self)
#         self.label.move(50,50)
#         
#         self.label1=QLabel('Java',self)
#         self.label1.move(100,100)
#         
#         self.label2=QLabel('Php',self)
#         self.label2.move(200,200)
        
        
        #create textbox
#         self.textbox=QLineEdit(self)
#         self.textbox.move(100,100)
#         self.textbox.resize(450,50)
#         self.textbox.setText("ok")
        
#         
#         #create button
#         self.button=QPushButton('Show text',self)
#         self.button.setToolTip("click here to show message!")
#         self.button.move(280,205)
#         self.button.clicked.connect(self.on_click)
        
        
        self.show()
        
#     @pyqtSlot()
#     def on_click(self):
#         textboxvalue=self.textbox.text()
#         QMessageBox.question(self,"Show message",'Message is: '+textboxvalue,QMessageBox.Ok,QMessageBox.Ok)
#         
        
#         Qt5 Messagebox
#         buttonReply = QMessageBox.question(self, 'PyQt5 message', "Do you like PyQt5?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
#         
#         if buttonReply==QMessageBox.Yes:
#             print("Yess clicked")
#         else:
#             print("Not clicked")
        
        
        
#         PyQt5 button
#         button=QPushButton('PyQt5 button', self)
#         button.setToolTip("This is an example button")
#         button.move(200,100)
#         button.clicked.connect(self.on_click)
    
        
        
#     Click action    
#     @pyqtSlot()
#     def on_click(self):
#         print('PyQt5 button click')
